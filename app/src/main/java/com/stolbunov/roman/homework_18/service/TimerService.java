package com.stolbunov.roman.homework_18.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.stolbunov.roman.homework_18.MainActivity;
import com.stolbunov.roman.homework_18.manager.IProgressListener;
import com.stolbunov.roman.homework_18.manager.TimeNotificationManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class TimerService extends Service {
    public static final int CONDITIONAL_WORK_COUNTER = 50;

    private long startTime;

    private IProgressListener listener;
    private ExecutorService executorService = Executors.newFixedThreadPool(1);

    @Override
    public void onCreate() {
        super.onCreate();
        startTime = System.currentTimeMillis();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        executorService.execute(new Notify());
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    class Notify implements Runnable {
        long timeDelta;
        int counter = 0;
        Intent intent = new Intent(MainActivity.BROADCAST_ACTION);

        @Override
        public void run() {
            setListener(new TimeNotificationManager(TimerService.this));
            while (counter <= CONDITIONAL_WORK_COUNTER) {
                timeDelta = System.currentTimeMillis() - startTime;
                intent.putExtra(MainActivity.TIMER_TIME, timeDelta);
                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                listener.progressListener(counter++);

                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            stopSelf();
        }
    }

    public void setListener(IProgressListener listener) {
        this.listener = listener;
    }
}

