package com.stolbunov.roman.homework_18.manager;

import android.app.Notification;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;

import com.stolbunov.roman.homework_18.R;
import com.stolbunov.roman.homework_18.service.TimerService;

import java.util.concurrent.TimeUnit;

public class TimeNotificationManager implements IProgressListener {
    private final int TIMER_NOTIFICATION = 654;
    private int counter = 0;

    private Context context;
    private android.app.NotificationManager nm;
    private NotificationCompat.Builder builder;

    public TimeNotificationManager(Context context) {
        this.context = context;
        nm = (android.app.NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(context);
    }

    @Override
    public void progressListener(long progress) {
        counter = (int) progress;
        nm.notify(TIMER_NOTIFICATION, buildNotification());

        if (progress == TimerService.CONDITIONAL_WORK_COUNTER) {
            try {
                TimeUnit.SECONDS.sleep(3);
                nm.cancel(TIMER_NOTIFICATION);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private Notification buildNotification() {
        builder.setContentTitle(context.getString(R.string.working))
                .setWhen(System.currentTimeMillis())
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setSmallIcon(R.mipmap.ic_launcher_round)
                .setTicker(context.getString(R.string.work_started))
                .setContentText(context.getString(R.string.work_process, (counter * 2)))
                .setProgress(TimerService.CONDITIONAL_WORK_COUNTER, counter, false);
        return builder.build();
    }
}
