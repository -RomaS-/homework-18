package com.stolbunov.roman.homework_18.manager;

public interface IProgressListener {
    void progressListener(long progress);
}
