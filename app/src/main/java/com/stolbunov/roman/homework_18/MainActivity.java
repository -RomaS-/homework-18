package com.stolbunov.roman.homework_18;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.stolbunov.roman.homework_18.service.TimerService;

public class MainActivity extends AppCompatActivity {
    public static final String BROADCAST_ACTION = "com.example.servicebindingsample.ui.screen.broadcast";
    public static final String TIMER_TIME = "TIMER_TIME";

    TextView timer;

    BroadcastReceiver receiver;
    LocalBroadcastManager broadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        timer = findViewById(R.id.txt_timer);

        Intent intent = new Intent(this, TimerService.class);
        startService(intent);

        initBroadcastReceiver();
    }

    private void initBroadcastReceiver() {
        broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                long time = intent.getLongExtra(TIMER_TIME, 0);
                timer.setText(String.valueOf(time));
            }
        };

        IntentFilter filter = new IntentFilter(BROADCAST_ACTION);
        broadcastManager.registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            broadcastManager.unregisterReceiver(receiver);
        }
    }

    ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("nik", "onServiceConnected: ");
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("nik", "onServiceDisconnected: ");
        }
    };
}
